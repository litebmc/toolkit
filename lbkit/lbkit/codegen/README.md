## README

#### build
```
python3 setup.py bdist_wheel
# 上传到无端仓, version需要替换成真实版本号
twine upload -r coding-pypi dist/lbkit-<version>.tar.gz
```

### install
```
# 从远端安装
pip3 install lbkit
# 或构建后从本地安装
pip3 install dist/lbkit-<version>-py3-none-any.whl
```