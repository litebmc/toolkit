## README

a tools set to manage components and products of litebmc:
1. generate code:  `lbkit gen` 命令可以读取idf(interface description file)文件用以生成c语言互操作代码。
2. build component: `litebmc build`命令可以构建符合符合litebmc实践的组件。
3. build product: `litebmc build`命令可用于构建集成rootfs和litebmc组件的产品rootfs镜像
