## 安装toolkit

toolkit由lbkit(pipy)和lbkit(deb & rpm)两部分组成：
1. lbkit(pypi)使用python编写，以pip包形式发布到pypi.org官方仓，使用命令`pip3 install lbkit`安装即可。
2. lbkit为rpm或deb包，当前已构建了ubuntu的发布仓，可以使用以下命令安装
```
# 添加软件源，将下面这一行内容追加到/etc/apt/sources.list
deb [arch=amd64] https://litebmc.com/ubuntu/ stable main
# 安装gpg公钥方法一：下载litebmc.gpg并安装
wget -qO- https://litebmc.com/ubuntu/litebmc.gpg | sudo tee /etc/apt/trusted.gpg.d/litebmc.asc
# 安装gpg公钥方法二：从keyserver.ubuntu.com下载ID为27B8C6BEC58DCF534A7BA35E1246430E1FC586BB的公钥
apt-key adv --keyserver keyserver.ubuntu.com --recv 27B8C6BEC58DCF534A7BA35E1246430E1FC586BB
# 查找uid为ci@litebmc.com的公钥ID是否为27B8C6BEC58DCF534A7BA35E1246430E1FC586BB，确保正确导入
apt-key list | grep 27B8
# 更新索引
apt update
# 安装lbkit
apt install lbkit
```

* 注: 暂未构建rpm包镜像仓，如非ubuntu，请参考下一章节构建rpm包并安装

## 构建与安装

当前支持在openSUSE Tumbleweed和ubuntu22.04系统构建rpm包和deb包，仅测试了上述系统的安装和使用，其它系统安装可能会失败

**openSUSE构建**

```shell
# 安装python3和python3-pip
zypper install python3 python3-pip
# 环境准备(需要root权限)
sudo ./build.py --prepare
# 构建rpm包
./build.py
# 安装pip包
pip3 install output/pypi/lbkit-0.6.0-py3-none-any.whl
# 安装rpm包(需要root权限)
sudo rpm -ivh output/rpm/x86_64/lbkit-0.6.0-1.x86_64.rpm
```

**ubuntu 22.04构建**

```shell
# 安装python3和python3-pip
apt install python3 python3-pip
# 环境准备(需要root权限)
sudo ./build.py --prepare
# 构建rpm包
./build.py
# 安装pip包
pip3 install output/pypi/lbkit-0.6.0-py3-none-any.whl
# 安装deb包(需要root权限)
sudo dpkg -i output/deb/x86_64/lbkit_0.6.0_all.deb

# 首次安装可能因依赖不存在导致失败，可以执行以下命令安装依赖
sudo apt install -f
```

## 系统初始化

**初始化conan配置**

litebmc的组件使用conan包管理器管理，当前支持的版本为conan==2.0.17版本，conan是lbkit的依赖并跟随主软件自动安装，首次使用需要配置conan仓：

```shell
# 初始化conan（会生成文件名为default的profile文件）
conan profile detect
# 创建conan远程仓链接并登录，需要根据实际仓库配置url、username、password
# conan remote add litebmc <url>
# conan remote login -p <password> -r litebmc <username>
# 以下命令为只读权限的仓库
conan remote add litebmc https://conan.litebmc.com/
conan remote login -p 301e0b354a95ca38d3c692219aaaf7c3 litebmc litebmc_ro
```

**修改pip源**

lbkit使用python开发，依赖pip包，可以更新合适的软件源提升安装速度，如清华源

```shell
pip config set global.index-url https://pypi.tuna.tsinghua.edu.cn/simple
```

**修改apt源**

如果使用ubuntu，可以更新合适的软件源，如aliyun

```shell
sed -i 's/archive.ubuntu/mirrors.aliyun/g' /etc/apt/sources.list
# 某些情况下可以禁用安装更新，如一次性的容器环境
# sed -i '/security/d' /etc/apt/sources.list
```