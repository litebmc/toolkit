#ifndef __UPGRADE_FIRMWARE_H__
#define __UPGRADE_FIRMWARE_H__

struct firmware_plugin {
    guint length;
    guint offset;
    guint persistent;
    gchar *filename;
};

#define ACTION_EXEC         1
#define ACTION_UPGRADE      2
struct firmware_step {
    guint length;
    guint offset;
    guint action;
    guint extension;
    gchar *filename;
};

struct firmware_crl {
    guint length;
    guint offset;
    gchar *filename;
};

struct firmware_info {
    struct firmware_crl crl;
    struct firmware_plugin plugin;
    struct firmware_step steps[32];
};
#define AES_KEY_LEN 32
#define AES_ENC_LEN 16
#define AES_IVC_LEN 16
#define RSA_ENC_LEN 512

#define MAX_FIRMWARE_HEADER 0x4000
#endif