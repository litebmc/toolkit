## 说明
lbpack是一个升级包签名工具，用于创建经CMS签名的升级包。

## 实现
签名用于保护我们的制品，常用于安全启动、安全升级等场景，为验证我们的制品是否被篡改，我们需要由一个我们信任的根证书验证我们的签名证书公钥，通过证书链验证合法的签名证书验证签名合法性，最终验证软件包。

我们不会对一个完整的软件包签名，因为这个包太大，相反，我们一般会使用hash算法计算出一个摘要信息，并使用签名算法签名摘要信息。
在验证软件包完整性时，我们可以使用根证书验证签名的软件包摘要未被篡改，然后使用相同算法计算出软件包摘要，两者相同就能证明软件包未被篡改。

为正确签名，我们需要根证书、签名证书、吊销列表、时间戳签名、待签名文件：
1. 根证书：根证书包含根公钥和根私钥，根公钥是开放可查的，而根私钥则是需要绝对保密的，根私钥泄露将摧毁整个信任链。我们一般指的根证书是指根公钥证书。根证书可以是开发者自签名的或者可信的第三方根证书，根证书需要集成在环境中，一般情况会将其公钥写入一次性写入的安全存储区域（如efuse、otp）。[参考](https://zh.wikipedia.org/zh-sg/%E6%A0%B9%E8%AF%81%E4%B9%A6)
2. 签名证书：正因为根证书的密钥保密性，我们不会使用根证书签名，而是使用签名证书，该证书由根证书签发。签名时需要使用签名证书的私钥，也需要注意保密。[参考](https://zh.wikipedia.org/wiki/%E4%BB%A3%E7%A0%81%E7%AD%BE%E5%90%8D)
3. 吊销列表（CRL）：当签名证书泄密时，我们需要使用CRL吊销该证书，CRL是也是根证书签名的，所以可以确保根证书的来源以及可信。[参考](https://zh.wikipedia.org/zh-sg/%E8%AF%81%E4%B9%A6%E5%90%8A%E9%94%80%E5%88%97%E8%A1%A8)
4. 时间戳签名：签名证书过期或被吊销后不被信任，但之间的签名应该认为有效，为此我们需要知道签名发生的具体时间并确保时间不会被篡改，该功能即为时间戳签名。

## 创建签名证书

#### 创建CA根证书
根公钥证书需要预置到环境中，用于验证签名证书的合法性。
```
# 创建配置文件
# 注意: 请修改req_distinguished_name证书名
cat << EOF > rootca.cnf
[ req ]
default_bits = 4096
distinguished_name = req_distinguished_name
prompt = no
default_md = sha256
string_mask = utf8only
x509_extensions = extensions

[ req_distinguished_name ]
C = CN
O = LiteBMC
CN = LiteBMC EnterpriseIT Root CA

[ extensions ]
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid:always, issuer
basicConstraints = CA:TRUE
keyUsage = nonRepudiation, cRLSign, keyCertSign
EOF

echo unique_subject=no > index.attr
# 创建rootca.pem证书公钥和rootca_pri.pem证书私钥文件
openssl req -new -nodes -utf8 -sha256 -days 36500 -batch -x509 -config rootca.cnf -outform PEM -out rootca.pem -keyout rootca_pri.pem
# 转换为der格式，非必要
# openssl x509 -in rootca.pem -inform pem -outform der -out rootca.der
```

#### 创建签名证书
签名证书由根证书签名，
```
cat << EOF > signer.cnf
[ req ]
default_bits = 4096
distinguished_name = req_distinguished_name
prompt = no
default_md = sha256
string_mask = utf8only
x509_extensions = extensions

[ req_distinguished_name ]
C = CN
O = HiFusion
CN = HiFusion EnterpriseIT S1

[ extensions ]
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid, issuer
basicConstraints = CA:FALSE
EOF

# 生成签名私鉏
openssl genrsa -out signer_pri.pem 4096
# 创建签名请求文件
openssl req -new -config signer.cnf -out signer.csr -key signer_pri.pem
# 请求CA根证书签名，生成signcert.pem
openssl x509 -req -in signer.csr -CA rootca.pem -CAkey rootca_pri.pem -CAcreateserial -out  signer_pub.pem -days 7300 -extensions extensions -extfile signer.cnf
# 合并signer_pub.pem和signer_pri.pem文件
cat signer_pub.pem signer_pri.pem > signer.pem
# 生成DER格式证书
# openssl x509 -in signcert.pem -inform pem -outform der -out signcert.der
```

#### 创建吊销列表

```
# 创建证书crl吊销列表
cat << EOF > crl.cnf
[ ca ]
default_ca=CA_default

[ CA_default ]
database=./index
crlnumber=./crlnumber
private_key=./rootca_pri.pem
crl_extensions=crl_ext
default_days=365
default_crl_days=30
default_md=default
preserve=no

[ crl_ext ]
authorityKeyIdentifier=keyid
EOF

touch index
echo 01 > crlnumber
openssl ca -gencrl -keyfile rootca_pri.pem -cert rootca.pem -out crl.pem -config crl.cnf
# 生成DER格式证书
# openssl crl -in crl.pem -inform pem -outform der -out ca.crl

## 示例：吊销一个签名证书
# openssl ca -revoke signer_pub.pem -cert rootca.pem -keyfile rootca_pri.pem -config crl.cnf
# openssl ca -gencrl -keyfile rootca_pri.pem -cert rootca.pem -out crl.pem -config crl.cnf
```

#### 创建时间戳证书
```
[ req ]
cat << EOF > crl.cnf
default_bits = 4096
distinguished_name = req_distinguished_name
prompt = no
default_md = sha256
string_mask = utf8only
x509_extensions = extensions

[ req_distinguished_name ]
C = CN
O = HiFusion
CN = HiFusion EnterpriseIT Timestamp Certificate

[ extensions ]
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid, issuer
basicConstraints = critical, CA:FALSE
keyUsage = critical, nonRepudiation, digitalSignature
extendedKeyUsage = critical, timeStamping

[ tsa ]
default_tsa = tsa_config1

[ tsa_config1 ]
serial = ./serial
crypto_device = builtin
signer_digest = sha256
default_policy  = 1.2.3.4.1
other_policies = 1.2.3.4.5.6, 1.2.3.4.5.7
digests = sha1, sha256, sha384, sha512
ess_cert_id_chain = no
ess_cert_id_alg = sha256
EOF

echo 03 > serial
# 创建时间戳签名证书私钥
openssl genrsa -out tsa_pri.pem 4096
# 创建签名请求文件
openssl req -new -config tsa.cnf -out tsa.csr -key tsa_pri.pem
# 使用根证书签名
openssl x509 -req -in tsa.csr -CA rootca.pem -CAkey rootca_pri.pem -CAcreateserial -out tsa_pub.pem -days 7300 -extensions extensions -extfile tsa.cnf
# 转换成DER格式证书
# openssl x509 -in tsa_pub.pem -inform pem -outform der -out tsa.der
# 生成tsa.pem
cat tsa_pub.pem tsa_pri.pem > tsa.pem
```