/*
 * Copyright (c) 2021-2023 xuhj@litebmc.com
 * litebmc is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *     http://license.coscl.org.cn/MulanPSL2
 **/
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <ctype.h>
#include <zlib.h>
#include <glib-2.0/glib.h>
#include <glib-2.0/gio/gio.h>
#include <openssl/rsa.h>
#include <openssl/aes.h>
#include <openssl/cms.h>
#include <openssl/pem.h>
#include <openssl/err.h>
#include "firmware.h"

struct firmware_global {
    guint package_id;
    guint product_id;
    guint product_mask;
    guint device_id;
    guint device_mask;
    guint extension;
    gchar *desc;
};

static struct firmware_global global = {0};

static gchar *glb_template_file = "template.ini";
static gchar *glb_tmp1_file = ".firmware.tmp1";
static gchar *glb_tmp2_file = ".firmware.tmp2";
static gchar *glb_output_file = "firmware.lbp";
static gchar *glb_sign_lbp_pri = "signer.pem";
static gchar *glb_enc_aes_pri = "aes_pri.pem";
static GOptionEntry cli_entries[] = {
    {"template", 't', 0, G_OPTION_ARG_STRING, &glb_template_file, "模板文件, default: template.ini", NULL},
    {"output", 'o', 0, G_OPTION_ARG_STRING, &glb_output_file, "输出镜像文件名, default: firmware.lbp", NULL},
    {"signer", 's', 0, G_OPTION_ARG_STRING, &glb_sign_lbp_pri, "LBP签名私钥, default: signer.pem", NULL},
    {"aes_pri", 'p', 0, G_OPTION_ARG_STRING, &glb_enc_aes_pri, "AES加密私钥, default: aes_pri.pem", NULL},
    {NULL}};

static guint32 _get_uint32_config(GKeyFile *cfg, const gchar *group, const gchar *key)
{
    GError *error = NULL;
    guint64 ullvalue = g_key_file_get_uint64(cfg, group, key, &error);
    if (error != NULL) {
        printf("read %s.%s failed, error:%s\n",group, key, error->message);
        exit(-1002);
    }
    return (guint32)(ullvalue & 0xffffffff);
}

static void print_cli_help(GOptionContext *context, gboolean cli_help, GOptionGroup *group)
{
    gchar *help;
    help = g_option_context_get_help(context, cli_help, group);
    g_print("%s", help);
    g_free(help);
}

static gboolean __check_filename(const gchar *filename)
{
    for (gint i = 0; i < strlen(filename); i++) {
        if (isalnum(filename[i]) || filename[i] == '.' || filename[i] == '_' || filename[i] == '-')
            continue;
        else
            return FALSE;
    }
    return TRUE;
}

static void load_template(const gchar *filename, struct firmware_info *firmware)
{
    GKeyFile *key_file = NULL;
    GError *error = NULL;
    gchar *str_val;
    gchar step[16] = {0};
    gint step_cnt = 0;

    memset(firmware, 0, sizeof(*firmware));

    key_file = g_key_file_new();
    if (!g_key_file_load_from_file(key_file, filename, G_KEY_FILE_NONE, &error)) {
        printf("load %s failed, error:%s\n",filename, error->message);
        g_error_free(error);
        g_key_file_free(key_file);
        exit(-2000);
    }
    // 读取global信息
    global.package_id = _get_uint32_config(key_file, "global", "package_id");
    global.product_id = _get_uint32_config(key_file, "global", "product_id");
    global.product_mask = _get_uint32_config(key_file, "global", "product_mask");
    global.device_id = _get_uint32_config(key_file, "global", "device_id");
    global.device_mask = _get_uint32_config(key_file, "global", "device_mask");
    global.extension = _get_uint32_config(key_file, "global", "extension");
    str_val = g_key_file_get_string(key_file, "global", "description", NULL);
    global.desc = str_val ? str_val : "";

    // 读取crl信息，必须存在
    str_val = g_key_file_get_string(key_file, "crl", "filename", NULL);
    if (!str_val || strlen(str_val) == 0) {
        printf("Read crl.filename failed\n");
        exit(-(__LINE__));
    }
    if (__check_filename(str_val) == FALSE) {
        printf("Filename %s has invalid char\n", str_val);
        exit(-(__LINE__));
    }
    firmware->crl.filename = str_val;

    // 读取plugin信息
    str_val = g_key_file_get_string(key_file, "plugin", "filename", NULL);
    if (str_val && strlen(str_val)) {
        if (__check_filename(str_val) == FALSE) {
            printf("Filename %s has invalid char\n", str_val);
            exit(-(__LINE__));
        }
        firmware->plugin.filename = str_val;
        firmware->plugin.persistent = _get_uint32_config(key_file, "plugin", "persistent");
    } else {
        firmware->plugin.filename = "";
    }
    for (int i = 0; i < G_N_ELEMENTS(firmware->steps); i++) {
        (void)sprintf(step, "step%d", i);
        if (!g_key_file_has_group(key_file, step))
            continue;
        firmware->steps[i].action = _get_uint32_config(key_file, step, "action");
        firmware->steps[i].extension = _get_uint32_config(key_file, step, "extension");
        str_val = g_key_file_get_string(key_file, step, "filename", NULL);
        if (!str_val || strlen(str_val) == 0) {
            printf("Read %s.filename failed\n", step);
            exit(-(__LINE__));
        }
        if (__check_filename(str_val) == FALSE) {
            printf("Filename %s has invalid char, step: %s\n", str_val, step);
            exit(-(__LINE__));
        }
        firmware->steps[i].filename = str_val;
        step_cnt++;
    }
    if (step_cnt == 0) {
        printf("Can't found steps infomation in file %s, exit.\n",filename);
        exit(-2002);
    }
    printf("Load template file %s successfully.\n",filename);
}

static guint append_file(FILE *fp, const gchar *filename, gboolean must_exist)
{
    FILE *step_file;
    gchar buf[16384];
    guint total_len = 0;
    gint read_len;

    step_file = fopen(filename, "rb");
    if (!step_file) {
        if (must_exist) {
            printf("Open file %s failed, exit.\n", filename);
            exit(-3000);
        }
        return 0;
    }
    while (1) {
        read_len = fread(buf, 1, sizeof(buf), step_file);
        if (read_len > 0) {
            if (fwrite(buf, read_len, 1, fp) != 1) {
                printf("Write file %s failed, err:%s\n", glb_tmp1_file, strerror(errno));
                fclose(step_file);
                exit(-3001);
            }
            total_len += (guint)read_len;
        } else {
            break;
        }
    }
    fclose(step_file);
    if (total_len == 0) {
        printf("File %s is empty, exit\n", filename);
        exit(-3002);
    }
    printf("Append %s successfully\n", filename);
    return total_len;
}

static void write_header(FILE *fp, struct firmware_info *firmware)
{
    GKeyFile *key_file = NULL;
    GError *error = NULL;
    gchar step[16] = {0};
    gchar *data = NULL;
    gsize length = 0;
    gint ret = 0;

    do {
        key_file = g_key_file_new();
        if (!g_key_file_load_from_data(key_file, "", 1, 0, &error)) {
            printf("load key file from data failed, error:%s\n", error->message);
            ret = -5000;
            break;
        }
        // 写入global信息
        g_key_file_set_uint64(key_file, "global", "package_id", (guint64)global.package_id);
        g_key_file_set_uint64(key_file, "global", "product_id", (guint64)global.product_id);
        g_key_file_set_uint64(key_file, "global", "product_mask", (guint64)global.product_mask);
        g_key_file_set_uint64(key_file, "global", "device_id", (guint64)global.device_id);
        g_key_file_set_uint64(key_file, "global", "device_mask", (guint64)global.device_mask);
        g_key_file_set_uint64(key_file, "global", "extension", (guint64)global.extension);
        g_key_file_set_string(key_file, "global", "description", global.desc);
        g_key_file_set_uint64(key_file, "crl", "length", firmware->crl.length);
        g_key_file_set_uint64(key_file, "crl", "offset", firmware->crl.offset);
        g_key_file_set_string(key_file, "crl", "filename", firmware->crl.filename);
        // 写入plugin信息
        g_key_file_set_string(key_file, "plugin", "filename", firmware->plugin.filename);
        g_key_file_set_uint64(key_file, "plugin", "length", firmware->plugin.length);
        g_key_file_set_uint64(key_file, "plugin", "offset", firmware->plugin.offset);
        g_key_file_set_uint64(key_file, "plugin", "persistent", firmware->plugin.persistent);
        // 写入steps信息
        for (int i = 0; i < G_N_ELEMENTS(firmware->steps); i++) {
            if (firmware->steps[i].length == 0)
                continue;
            (void)sprintf(step, "step%d", i);
            g_key_file_set_uint64(key_file, step, "length", firmware->steps[i].length);
            g_key_file_set_uint64(key_file, step, "offset", firmware->steps[i].offset);
            g_key_file_set_uint64(key_file, step, "action", firmware->steps[i].action);
            g_key_file_set_uint64(key_file, step, "extension", firmware->steps[i].extension);
            g_key_file_set_string(key_file, step, "filename", firmware->steps[i].filename);
        }
        data = g_key_file_to_data(key_file, &length, &error);
        if (data == NULL || length == 0) {
            printf("Save firmware header infomation to data failed, error:%s\n", error->message);
            ret = -5002;
            break;
        }
        if (length > MAX_FIRMWARE_HEADER) {
            printf("Firmware header too bigger, must less than %d, real:%u\n", MAX_FIRMWARE_HEADER - 4, (guint)length);
            ret = -5003;
            break;
        }
        fseek(fp, 0, SEEK_SET);
        if (fwrite(&length, 4, 1, fp) != 1) {
            printf("Write Firmware header length failed, error:%s\n", strerror(errno));
            ret = -5004;
            break;
        }
        if (fwrite(data, length, 1, fp) != 1) {
            printf("Write Firmware header information failed, error:%s\n", strerror(errno));
            ret = -5005;
            break;
        }
        printf("Firmware header info:\n");
        printf("=====================\n");
        printf("%s", data);
        printf("=====================\n");
    } while (0);
    g_key_file_free(key_file);
    g_free(data);
    if (error)
        g_error_free(error);
    if (ret)
        exit(ret);
}

static gint make_zlib_package(FILE *tmp_fp)
{
    gzFile gz;
    gchar buf[16384];
    gint ret;
    gint result = -7001;

    gz = gzopen(glb_tmp2_file, "w");
    if (!gz) {
        printf("open gz file %s failed\n", glb_tmp2_file);
        return -7000;
    }
    fseek(tmp_fp, 0, SEEK_SET);
    while (1) {
        ret = fread(buf, 1, sizeof(buf), tmp_fp);
        if (ret > 0) {
            if (gzwrite(gz, buf, ret) == ret) {
                result = 0;
            } else {
                result = -7002;
                break;
            }
        } else {
            break;
        }
    }
    gzclose(gz);
    printf("make zlib package return: %d\n", result);
    return result;
}

static gint append_aes_key(FILE *aes_fp, unsigned char *aes_pri, size_t aes_len)
{
    unsigned char enc_buf[RSA_ENC_LEN] = {0};
    size_t enc_len = sizeof(enc_buf);
    FILE *rsa_fp;
    EVP_PKEY *rsa_pri = NULL;

    rsa_fp = fopen(glb_enc_aes_pri, "rb");
    if (!rsa_fp) {
        printf("Open private key %s failed\n", glb_enc_aes_pri);
        return -(__LINE__);
    }
    rsa_pri = PEM_read_PrivateKey(rsa_fp, NULL, NULL, NULL);
    fclose(rsa_fp);
    if (!rsa_pri) {
        ERR_print_errors_fp(stdout);
        return -(__LINE__);
    }
    EVP_PKEY_CTX *ctx = EVP_PKEY_CTX_new(rsa_pri, NULL);
    EVP_PKEY_encrypt_init(ctx);

    enc_len = EVP_PKEY_encrypt(ctx, enc_buf, &enc_len, aes_pri, aes_len);
    if (enc_len != sizeof(enc_buf)) {
        EVP_PKEY_free(rsa_pri);
        ERR_print_errors_fp(stdout);
        return -(__LINE__);
    }

    EVP_PKEY_free(rsa_pri);
    if (fwrite(enc_buf, enc_len, 1, aes_fp) != 1) {
        return -(__LINE__);
    }
    return 0;
}

static gint make_aes_package(void)
{
    unsigned char aes_pri[AES_KEY_LEN] = {0};
    unsigned char in_buf[16384];
    unsigned char out_buf[16384];
    gint out_len = 0;
    FILE *fp, *gz_fp, *aes_fp;
    gint ret;
    gint result = -8000;
    unsigned char aes_ivec[AES_IVC_LEN] = {0xef, 0x4e, 0x68, 0xea, 0x43, 0xe6, 0x54, 0xd6,
        0xe3, 0x6c, 0xef, 0x2e, 0xce, 0x41, 0x8f, 0xae
    };

    // 读取32字节随机数做为加密密码
    fp = fopen("/dev/random", "rb");
    if (!fp) {
        printf("Open /dev/random failed, err:%s\n", strerror(errno));
        return -8000;
    }
    ret = fread(aes_pri, 1, AES_KEY_LEN, fp);
    fclose(fp);
    if (ret != AES_KEY_LEN) {
        printf("Read aes key failed, err:%s\n", strerror(errno));
        return -8001;
    }
    // 读取gz文件，AES256加密后写入加密文件
    gz_fp = fopen(glb_tmp1_file, "rb");
    if (!gz_fp) {
        printf("Open file %s failed, error:%s\n", glb_tmp1_file, strerror(errno));
        return -8002;
    }
    EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new();
    do {
        aes_fp = fopen(glb_tmp2_file, "w+");
        if (!aes_fp) {
            printf("Open file %s failed, error:%s\n", glb_tmp2_file, strerror(errno));
            result = -8003;
            break;
        }
        if (EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, aes_pri, aes_ivec) != 1) {
            printf("Init evp encrypt failed");
            result = -8004;
            break;
        }
        while (1) {
            ret = fread(in_buf, 1, sizeof(in_buf), gz_fp);
            if (ret <= 0) {
                break;
            }
            ret = ((ret -1 )/ AES_ENC_LEN + 1) * AES_ENC_LEN;
            out_len = sizeof(out_buf);
            if (EVP_EncryptUpdate(ctx, (unsigned char *)out_buf, (gint *)&out_len, in_buf, ret) != 1) {
                printf("Encrypt update failed, error:%s\n", strerror(errno));
                break;
            }
            if (fwrite(out_buf, out_len, 1, aes_fp) != 1) {
                printf("Write aes file failed, error:%s\n", strerror(errno));
                break;
            }
        }
        if (!feof(gz_fp))
            break;
        // 使用固件加密密钥RSA4096加密AES密钥后追加后文件末尾
        result = append_aes_key(aes_fp, aes_pri, sizeof(AES_KEY_LEN));
    } while (0);
    EVP_CIPHER_CTX_free(ctx);
    fclose(gz_fp);
    if (aes_fp) {
        fclose(aes_fp);
        rename(glb_tmp2_file, glb_tmp1_file);
    }
    return result;
}

static guint8 *__cms_sign(gchar *data, gint data_len, gint *sig_len)
{
    BIO *in = NULL, *out = NULL, *tbio = NULL;
    X509 *scert = NULL;
    EVP_PKEY *skey = NULL;
    CMS_ContentInfo *cms = NULL;
    guint8 *sig = NULL, *sig_out;

    *sig_len = 0;
    /* Read in signer certificate and private key */
    tbio = BIO_new_file(glb_sign_lbp_pri, "r");
    if (!tbio)
        goto err;

    scert = PEM_read_bio_X509(tbio, NULL, 0, NULL);
    BIO_reset(tbio);
    skey = PEM_read_bio_PrivateKey(tbio, NULL, 0, NULL);
    if (!scert || !skey)
        goto err;

    /* Open content being signed */
    in = BIO_new_mem_buf(data, data_len);
    if (!in)
        goto err;

    int flags = CMS_DETACHED | CMS_NOSMIMECAP | CMS_BINARY;
    /* Sign content */
    cms = CMS_sign(scert, skey, NULL, in, flags);
    if (!cms)
        goto err;

    out = BIO_new(BIO_s_mem());
    /* Write out S/MIME message */
    if (!i2d_CMS_bio_stream(out, cms, in, flags))
        goto err;

    *sig_len = (gint)BIO_get_mem_data(out, &sig);
    printf("%02x%02x %02x%02x\n", sig[0], sig[1], sig[2], sig[3]);
    printf("signature length:%d\n", *sig_len);

 err:
    if (!sig) {
        fprintf(stderr, "Error Signing Data\n");
        ERR_print_errors_fp(stderr);
    } else {
        sig_out = g_memdup2(sig, *sig_len);
    }
    CMS_ContentInfo_free(cms);
    X509_free(scert);
    EVP_PKEY_free(skey);
    BIO_free(in);
    BIO_free(out);
    BIO_free(tbio);
    return sig_out;
}

static gchar *get_lbp_digest(void)
{
    FILE *fp;
    gchar buf[16384];
    gint ret;
    gchar *sum;

    fp = fopen(glb_tmp1_file, "rb");
    if (!fp) {
        printf("Open file %s failed, error:%s\n", glb_tmp1_file, strerror(errno));
        return NULL;
    }
    GChecksum *sha256_sum = g_checksum_new(G_CHECKSUM_SHA256);
    while (1) {
        ret = fread(buf, 1, sizeof(buf), fp);
        if (ret > 0)
            g_checksum_update(sha256_sum, (const guchar *)buf, ret);
        else
            break;
    }
    if (!feof(fp)) {
        fclose(fp);
        return NULL;
    }
    fclose(fp);
    sum = g_strdup(g_checksum_get_string(sha256_sum));
    g_checksum_free(sha256_sum);
    return sum;
}

static int make_lbp_package(void)
{
    GKeyFile *key_file = g_key_file_new();
    gint sig_len;
    guint8 *sig_dat;
    gsize length;
    gchar *data;
    GError *error = NULL;
    FILE *fp;

    if (!g_key_file_load_from_data(key_file, "", 1, 0, &error)) {
        printf("load key file from data failed, error:%s\n", error->message);
        g_error_free(error);
        return -(__LINE__);
    }
    // 写入signature信息
    data = get_lbp_digest();
    g_key_file_set_string(key_file, "signature", "manufacturer", "HiFusion");
    g_key_file_set_string(key_file, "signature", "digest-sha256", data);
    g_free(data);
    data = g_key_file_to_data(key_file, &length, NULL);
    g_key_file_free(key_file);
    if (!data || length <= 0) {
        printf("Get header info failed.\n");
        return -(__LINE__);
    }

    fp = fopen(glb_tmp1_file, "a");
    if (!fp) {
        printf("Open file %s failed, error:%s\n", glb_tmp1_file, strerror(errno));
        return -(__LINE__);
    }
    fwrite(data, length, 1, fp);
    // 写入header长度信息
    fwrite(&length, 4, 1, fp);
    sig_dat = __cms_sign(data, length, &sig_len);
    g_free(data);
    length = 0;
    if (!sig_dat)
        goto error;
    printf("sig_dat: %02x%02x %02x%02x\n", sig_dat[0], sig_dat[1], sig_dat[2], sig_dat[3]);
    fwrite(sig_dat, sig_len, 1, fp);
    // 写入signature长度信息
    fwrite(&sig_len, 4, 1, fp);

    fclose(fp);
    rename(glb_tmp1_file, glb_output_file);
    return 0;
error:
    unlink(glb_tmp1_file);
    exit(-(__LINE__));
}

static void make_package(struct firmware_info *firmware)
{
    FILE *fp;
    // 预留16K用于写入头部数据
    guint append_offset = MAX_FIRMWARE_HEADER;
    guint append_len;
    gint ret;

    fp = fopen(glb_tmp1_file, "w+");
    fseek(fp, append_offset, SEEK_SET);

    append_len = append_file(fp, firmware->crl.filename, TRUE);
    firmware->crl.length = append_len;
    firmware->crl.offset = append_offset;
    append_offset += append_len;
    append_len = append_file(fp, firmware->plugin.filename, FALSE);
    firmware->plugin.length = append_len;
    firmware->plugin.offset = append_offset;
    append_offset += append_len;
    for (int i = 0; i < G_N_ELEMENTS(firmware->steps); i++) {
        if (!firmware->steps[i].filename)
            continue;
        append_len = append_file(fp, firmware->steps[i].filename, TRUE);
        firmware->steps[i].length = append_len;
        firmware->steps[i].offset = append_offset;
        append_offset += append_len;
    }
    write_header(fp, firmware);
    ret = make_zlib_package(fp);
    fclose(fp);
    rename(glb_tmp2_file, glb_tmp1_file);
    ret = make_aes_package();
    if (ret != 0) {
        exit(ret);
    }
    ret = make_lbp_package();
    if (ret != 0) {
        exit(ret);
    }
}

int main(gint argc, char *argv[])
{
    GOptionContext *context;
    GError *error = NULL;
    struct firmware_info firmware = {0};

    OpenSSL_add_all_algorithms();
    ERR_load_crypto_strings();
    context = g_option_context_new("make lbp package");
    g_option_context_set_help_enabled(context, FALSE);
    g_option_context_add_main_entries(context, cli_entries, NULL);
    if (!g_option_context_parse(context, &argc, &argv, &error))
    {
        print_cli_help(context, TRUE, NULL);
        g_error_free (error);
        g_option_context_free (context);
        return -1;
    }
    load_template(glb_template_file, &firmware);
    make_package(&firmware);
}
