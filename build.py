#!/usr/bin/python3
import os
import shutil
import argparse
import subprocess
import platform
import re


class Build(object):
    """toolkit构建"""
    def __init__(self, version):
        self.cwd = os.getcwd()
        self.version = version

    def prepare_env(self):
        self.temp_dir = os.path.join(self.cwd, ".temp")
        self.output = os.path.join(self.cwd, "output")
        self.pypi_output = os.path.join(self.output, "pypi")
        self.rpm_output = os.path.join(self.output, "rpm")
        self.deb_output = os.path.join(self.output, "deb", platform.machine())
        self.data_dir = os.path.join(self.temp_dir, "data")
        self.share_dir = os.path.join(self.data_dir, "usr", "share", "litebmc")
        self.bin_dir = os.path.join(self.data_dir, "usr", "bin")
        if os.path.exists(self.temp_dir):
            shutil.rmtree(self.temp_dir)
        if os.path.exists(self.output):
            shutil.rmtree(self.output)
        os.makedirs(self.temp_dir)
        os.makedirs(self.output)
        os.makedirs(self.deb_output)
        os.makedirs(self.rpm_output)
        os.makedirs(self.share_dir)
        os.makedirs(self.bin_dir)

    @property
    def os_name(self):
        for issue in ["/etc/issue", "/etc/issue.net"]:
            if not os.path.isfile(issue):
                continue
            fp = open(issue, "r")
            content = fp.read()
            if "openSUSE Tumbleweed" in content:
                return "opensuse"
            if "Ubuntu 22.04" in content:
                return "ubuntu:22.04"
            if "Alpine Linux 3" in content:
                return "alpine3"
            fp.close()
        raise OSError("Unsupported system get, only ubuntu 22.04 and opensuse tumbleweed supported")

    def make_lbpack(self):
        """构建lbpack并将制品复制到打包目录"""
        lbpack_dir = os.path.join(self.temp_dir, "lbpack")
        shutil.copytree(os.path.join(self.cwd, "lbpack"), lbpack_dir)
        os.chdir(lbpack_dir)
        subprocess.run("cmake .", shell=True, check=True)
        subprocess.run("make", shell=True, check=True)
        dst = os.path.join(self.bin_dir, "lbpack")
        shutil.copyfile("lbpack", dst)
        os.chmod(dst, 0o555)
        dst = os.path.join(self.share_dir, "qemu.conf")
        shutil.copyfile("qemu.conf", dst)
        os.chmod(dst, 0o644)
        shutil.copyfile("template.ini", os.path.join(self.share_dir, "lbpack_template.ini"))
        os.chdir(self.cwd)

    def make_lbkit(self):
        """构建lbkit并将制品复制到打包目录"""
        lbkit_dir = os.path.join(self.temp_dir, "lbkit")
        shutil.copytree(os.path.join(self.cwd, "lbkit"), lbkit_dir)
        os.chdir(lbkit_dir)
        version_file = os.path.join(os.getcwd(), "lbkit", "__init__.py")
        with open(version_file, "w", encoding="utf-8") as fp:
            fp.write(f"\n__version__ = '{self.version}'\n")
        shutil.rmtree("dist", ignore_errors=True)
        subprocess.run(f"python3 -m build --outdir {self.pypi_output}", shell=True, check=True)
        os.chdir(self.cwd)

    def pack_schema(self):
        dst = os.path.join(self.share_dir, "schema")
        shutil.copytree("schema", dst)

    def pack_targets(self):
        dst = os.path.join(self.share_dir, "targets")
        shutil.copytree("targets", dst)

    def pack_deb(self):
        """打包deb包"""
        print("Start pack deb")
        deb_dir = os.path.join(self.temp_dir, "deb")
        debian = os.path.join(deb_dir, "DEBIAN")
        shutil.copytree(os.path.join(self.cwd, "build", "DEBIAN"), debian, dirs_exist_ok=True)
        shutil.copytree(self.data_dir, deb_dir, dirs_exist_ok=True)
        control = os.path.join(debian, "control")
        cmd = f'sed -i "s@<version>@{self.version}@g" {control}'
        subprocess.run(cmd, shell=True, check=True)
        subprocess.run(f"dpkg -b {deb_dir} {self.deb_output}/lbkit_{self.version}_all.deb", shell=True, check=True)

    def pack_rpm(self, distro):
        """打包rpm包"""
        print("Start pack rpm")
        rpm_dir = os.path.join(self.temp_dir, distro)
        buildroot_dir = os.path.join(rpm_dir, "FILES")
        spec_dir = os.path.join(rpm_dir, "SPECS")
        os.makedirs(spec_dir)
        os.makedirs(buildroot_dir)
        shutil.copytree(self.data_dir, buildroot_dir, dirs_exist_ok=True)

        spec = os.path.join(spec_dir, distro + ".spec")
        shutil.copyfile(os.path.join(self.cwd, "build", distro, distro + ".spec"), spec)
        shutil.copytree(self.data_dir, rpm_dir, dirs_exist_ok=True)
        cmd = f'sed -i "s@<version>@{self.version}@g" {spec}'
        subprocess.run(cmd, shell=True, check=True)
        os.chdir(rpm_dir)
        rpm_output = os.path.join(self.rpm_output, distro)
        os.makedirs(rpm_output, exist_ok=True)
        subprocess.run(f"rpmbuild -bb {spec} --buildroot={buildroot_dir} --define \"_rpmdir {rpm_output}\"", shell=True, check=True)
        os.chdir(self.cwd)

    def prepare(self):
        if self.os_name == "opensuse":
            subprocess.run("pip3 install build wheel --break-system-packages", shell=True, check=True)
            pkg = ["git", "pkgconf", "glib2-devel", "python3-venv", "cmake", "libopenssl-3-devel", "dpkg", "rpm-build", "gcc", "systemd-devel", "lcov"]
            cmd = "zypper install -y %s" % (" ".join(pkg))
        elif self.os_name == "ubuntu:22.04":
            subprocess.run("pip3 install build wheel", shell=True, check=True)
            pkg = ["dbus-x11", "git", "python3-venv", "pkgconf", "libglib2.0-dev", "cmake", "libssl-dev", "dpkg", "rpm", "gcc", "libsystemd-dev", "lcov"]
            cmd = "apt install -y %s" % (" ".join(pkg))
        elif self.os_name == "alpine3":
            subprocess.run("pip install build wheel --break-system-packages", shell=True, check=True)
            pkg = ["dbus-x11", "git", "pkgconf", "python3-venv", "glib-dev", "cmake", "make", "openssl-dev", "dpkg", "rpm", "gcc", "musl-dev"]
            cmd = "apk add %s --no-cache" % (" ".join(pkg))
        subprocess.run(cmd, check=True, shell=True)

    def test(self):
        os.chdir(os.path.join(self.cwd, "lbkit"))
        subprocess.run(["python3", "-m", "unittest", "discover", "-v"], check=True)
        os.chdir(self.cwd)

    def run(self):
        """启动构建"""
        self.prepare_env()
        self.make_lbpack()
        self.make_lbkit()
        self.pack_schema()
        self.pack_targets()
        self.pack_deb()
        self.pack_rpm("rockylinux")
        self.pack_rpm("opensuse")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Build lbkit(litebmc-toolkit)")
    parser.add_argument("--prepare", help='install build dependencies', action="store_true")
    parser.add_argument("-t", "--test", help='test package', action="store_true")
    # do not need modify this version when need release
    # use [toolkit_release](https://jenkins.litebmc.com/job/toolkit_release) with TOOLKIT_TAG please
    parser.add_argument("--version", help='lbkit verson', default="0.6.0")
    args = parser.parse_args()
    if re.match("^(0|([1-9][0-9]*))\.(0|([1-9][0-9]*))\.(0|([1-9][0-9]*))$", args.version) is None:
        raise Exception("version error, not match regex: ^(0|([1-9][0-9]*))\.(0|([1-9][0-9]*))\.(0|([1-9][0-9]*))$")
    b = Build(args.version)
    if args.prepare:
        b.prepare()
    elif args.test:
        b.test()
    else:
        b.run()
