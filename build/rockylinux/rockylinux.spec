Name: lbkit
Version: <version>
Release: 1%{?dist}
Summary: litebmc developer's kit
License: Mulan PSL v2
Requires: cmake, openssl-devel, zlib, python3-pip, git-core, pkgconf, glib2-devel, systemd-devel, lcov, python3, gcc, gcc-c++, kpartx, fuse, e2fsprogs, rsync

%description
litebmc developer's kit, include lbpack(package tool), lbkit(cli command)

%postun
if [ "$1" == 0 ]; then
    echo "remote files"
    rm /usr/share/litebmc -rf
    rm /usr/bin/lbpack -f
fi

%files
/usr/bin/*
/usr/share/litebmc/*
